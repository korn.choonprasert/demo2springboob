package com.sara.demo2.Controller;


import com.sara.demo2.Service.MemberService;
import com.sara.demo2.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DemoController {

    @Autowired
    MemberService memberService;

    @GetMapping(path = "/test/member")
    public ResponseEntity<List<Member>> getAllMember() {

        return new ResponseEntity<>(memberService.takealldemo2(), HttpStatus.OK);
    }

}
