package com.sara.demo2.Service;


import com.sara.demo2.entity.Member;
import com.sara.demo2.repository.MemberRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MemberService {

    @Autowired
    MemberRepo memberRepo;

    public List<Member> takealldemo2() {
        return memberRepo.findAll();
    }
}
