package com.sara.demo2.repository;

import com.sara.demo2.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MemberRepo extends JpaRepository<Member, Integer> {

    List<Member> findAll();

}
